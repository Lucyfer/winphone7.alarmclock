﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO.IsolatedStorage;
using System.IO;
using Microsoft.Phone.Controls;

namespace PhoneApp1
{
    public partial class Logs : PhoneApplicationPage
    {
        public Logs()
        {
            InitializeComponent();
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            listBox1.Items.Clear();
            try
            {
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (store.FileExists("log.log"))
                        using (var stream = new IsolatedStorageFileStream("log.log", FileMode.Open, store))
                        {
                            using (var f = new StreamReader(stream))
                            {
                                while (!f.EndOfStream)
                                    listBox1.Items.Add(f.ReadLine());
                            }
                        }
                }
            }
            catch (Exception) { }
        }
    }
}