﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Scheduler;

namespace PhoneApp1
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private void Reset() {
            textBox1.Text = "Don't forget ";
            var d = DateTime.Now.AddSeconds(60);
            textBox2.Text = d.ToString("M/dd/yyyy");
            textBox3.Text = d.ToString("HH:mm:ss");
        }

        private void Save(string mes)
        {
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (var stream = new IsolatedStorageFileStream("log.log", FileMode.Append, store))
                {
                    using (var f = new StreamWriter(stream))
                        f.WriteLine(mes);
                }
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var alarmtime = string.Format("{0} {1}", textBox2.Text, textBox3.Text);
                var d = DateTime.Parse(alarmtime);
                var rem = new Reminder(alarmtime);
                rem.BeginTime = d;
                rem.Content = textBox1.Text;
                rem.Title = "Alarm!";
                rem.ExpirationTime = d.AddHours(2);
                ScheduledActionService.Add(rem);
                Save(string.Format("{0} - {1}", alarmtime, textBox1.Text));
                Reset();
                MessageBox.Show("Successfully added", "Info", MessageBoxButton.OK);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        /// <summary>log</summary>
        private void ApplicationBarMenuItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Logs.xaml", UriKind.Relative));
        }

        /// <summary>clear all</summary>
        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
        }

        /// <summary>defaults</summary>
        private void ApplicationBarIconButton_Click_1(object sender, EventArgs e)
        {
            Reset();
        }
    }
}